module Blog
  module BlogHelpers

    def find_post(id)
      @post = Post.find_by_id(id)
      unless @post
        error!({'Error' => '', 'Detail' => "No Post with id of #{id}"}, 404)
      end

      # Return
      @post
    end
  end
end
