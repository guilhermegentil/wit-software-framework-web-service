require 'grape'
require_relative './blog_helpers'

module Blog

  class API < Grape::API

    format :json
    helpers Blog::BlogHelpers

		resource :weblogs do

      # Response to GET /api/weblogs
			get do
        if params[:type] == 'newest'
          Weblog.order("created_at DESC")[0]
        elsif params[:type] == 'empty'
          Weblog.all.select { |w| w.posts.count == 0 }
        else
          Weblog.all
        end
			end

      # Response to POST /api/weblogs
      post do
        @weblog = Weblog.new
        @weblog.title = params[:title] if params[:title]
        @weblog.description = params[:description] if params[:description]
        @weblog.save

        # HTTP status created
        status 201

        # Return
        @weblog
      end

      # Validation for all namespaces
      params do
        requires :id, type: Integer
      end

      namespace ':id' do

        get do
         @weblog = Weblog.find_by_id(params[:id])
         unless @weblog
           error!({'Error' => 'Invalid resource id', 'Detail' => "No weblog with id of #{params[:id]}"}, 404)
         end
         @weblog
        end

        delete do
          @weblog = Weblog.find_by_id(params[:id])
          if @weblog
            @weblog.destroy
          else
            error!({'Error' => 'Invalid resource id', 'Detail' => "No weblog with id of #{params[:id]}"}, 404)
          end
        end

        # Validation of PUT parameters
        params do 
          requires :title, type: String
          requires :description, type: String
        end

        put do
          @weblog = Weblog.find_by_id(params[:id])
          if @weblog
            @weblog.title = params[:title]
            @weblog.description = params[:description]
            @weblog.save

            # Return
            @weblog
          else
            error!({'Error' => 'Invalid resource id', 'Detail' => "No weblog with id of #{params[:id]}"}, 404)
          end
        end

        namespace 'posts' do
          # Response to GET /api/weblogs/:id/posts 
          get do
            @weblog = Weblog.find_by_id(params[:id])
            if @weblog
              #Return
              @weblog.posts
            else
              error!({'Error' => 'Invalid resource id', 'Detail' => "No weblog with id of #{params[:id]}"}, 404)
            end
          end

          delete do
            @weblog = Weblog.find_by_id(params[:id])
            if @weblog
              @weblog.posts.clear
            else
              error!({'Error' => 'Invalid resource id', 'Detail' => "No weblog with id of #{params[:id]}"}, 404)
            end
          end          

          # Validation of POST parameters
          params do
            requires :title, type: String
            requires :body, type: String
          end

          # Response to POST /api/weblogs/:id/posts
          # Create a new post for a user
          post do
            @weblog = Weblog.find_by_id(params[:id])
            if @weblog
              @post = @weblog.posts.create(title: params[:title], body: params[:body])
              # HTTP code, created
              status 201 
              # Return
              @post
            else
              # HTTP code, page not found
              error!({'Error' => "Invalid resource id", 'Detail' => "No weblog with id of #{params[:id]}"}, 404)
            end
          end
        end
      end
		end # resource :weblog

    resource :posts do
      
      # GET /api/posts
      get do
        Post.all
      end

      # Validation
      params do
        requires :id, type: Integer
      end
      
      # Use the above validation for all endpoints inside this namespace
      namespace ':id' do
        
        # GET /api/posts/:id
        get do
          @post = find_post(params[:id])
        
          unless @post 
            error!({'Error' => 'Invalid resource id', 'Detail' => "No post with id of #{params[:id]}"}, 404)
          end

          #Return
          @post
        end

         # DELETE /api/posts/:id, delete a single post
        delete do
          @post = find_post(params[:id])

          if @post
           @post.destroy
          else
           error!({'Error' => 'Invalid resource id', 'Detail' => "No post with id of #{params[:id]}"}, 404)
          end
        end

        namespace 'comments' do
          # Validation
          params do
            optional :name
            requires :body, type: String
          end

          # POST /api/posts/:id/comments
          post do
            @post = find_post(params[:id])

            if @post
              @comment = @post.comments.create(name: params[:name], body: params[:body])

              # HTTP status 201, created
              status 201

              #Return
              @comment
            else
              error!({'Error' => 'Invalid resource id', 'Detail' => "No post with id of #{params[:id]}"}, 404)
            end
          end

          # GET /api/posts/:id/comments
          get do
            @post = find_post(params[:id])

            # Return
            @post.comments
          end

          # DELETE /api/posts/:id/comments
          delete do
            @post = find_post(params[:id])
            @post.comments.clear
          end

        end # namespace comments
      end # namespace :id

      # DELETE /api/posts, delete all posts
      delete do
        Post.destroy.all
      end

    end # resource :posts

    resource :comments do
      # GET all comments
      get do
        Comment.all
      end

      # Validation
      params do
        requires :id, type: Integer
      end

      # Match URL /api/comments/:id
      namespace ':id' do
        # Get all comments
        get do
          @comment = Comment.find_by_id(params[:id])
          unless @comment
            error!({'Error' => 'Invalid resource id', 'Detail' => "No comment with id of #{params[:id]}"}, 404)
          end

          #return
          @comment
        end

        # Delete a comment
        delete do
          @comment = Comment.find_by_id(params[:id])

          if @comment
            @comment.destroy
          else
            error!({'Error' => 'Invalid resource id', 'Detail' => "No comment with id of #{params[:id]}"}, 404)
          end
        end

        params do
          optional :name
          requires :body, type: String
        end

        # Update a existing comment
        put do
          @comment = Comment.find_by_id(params[:id])

          if @comment
            @comment.name = params[:name]
            @comment.body = params[:body]
          else
            error!({'Error' => 'Invalid resource id', 'Detail' => "No comment with id of #{params[:id]}"}, 404)
          end

          #return
          @comment
        end

      end
    end

	end

end